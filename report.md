# mysql report data

## setup

### login
(as root) mysql -u root -p

### create databases
* create database bigdata;
* create table names (id INT, name CHAR(10));
* load data infile '/home/foobar/code/sql/names.csv' into table names fields terminated by ',';

* create table cities(id INT, name CHAR(10));
* load data infile '/home/foobar/code/sql/cities.csv' into table cities fields terminated by ',';

* create table jobs(id INT, name CHAR(10));
* load data infile '/home/foobar/code/sql/jobs.csv' into table jobs fields terminated by ',';

* create table games (id INT, name CHAR(20), studio_id INT, console CHAR(20), date INT);
* load data local infile '/home/foobar/Documents/data/games.csv' into table games fields terminated by ',' lines terminated by '\n' ignore 1 lines ;

* create table studios (studio_id INT, name CHAR(20), birth INT, location CHAR(20));
* load data local infile '/home/foobar/Documents/data/studios.csv' into table studios fields terminated by ',' lines terminated by '\n' ignore 1 lines ;

### rename column name
* alter table cities rename column name to city;

### use db
use bigdata;

## data 

### select * from names ;
```
+------+--------+
| id   | name   |
+------+--------+
|    1 | Paul   |
|    2 | Joe    |
|    3 | Claire |
|    4 | Sara   |
+------+--------+
```

```
### select * from cities;
+------+------------+
| id   | city       |
+------+------------+
|    1 | Paris      |
|    2 | London     |
|    3 | Paris      |
|    4 | Porto Rico |
+------+------------+
```


### select * from jobs;
```
+------+---------+
| id   | job     |
+------+---------+
|    1 | Plumber |
|    2 | Chef    |
|    3 | Driver  |
|    4 | Writer  |
+------+---------+
```

### show all data

select name,job,city from names, jobs, cities where names.id=cities.id and names.id=jobs.id;

```
+--------+---------+------------+
| name   | job     | city       |
+--------+---------+------------+
| Paul   | Plumber | Paris      |
| Joe    | Chef    | London     |
| Claire | Driver  | Paris      |
| Sara   | Writer  | Porto Rico |
+--------+---------+------------+
```

### show the names of all people in Paris

select name from names, cities where names.id = cities.id and cities.city = 'Paris';

```
+--------+
| name   |
+--------+
| Paul   |
| Claire |
+--------+
```

### names of all plumbers

select name from names, jobs where names.id=jobs.id and job = 'Plumber';

### number of people in paris

select count(*) from names, cities where names.id = cities.id and cities.city = 'Paris';

### rename colmumn

select name as NEWNAME from names, cities where names.id = cities.id and cities.city = 'Paris';

```
+---------+
| NEWNAME |
+---------+
| Paul    |
| Claire  |
+---------+
```


## joins

### data

#### select * from studios;
```
+-----------+-------------+-------+----------+
| studio_id | name        | birth | location |
+-----------+-------------+-------+----------+
|         1 | square_enix |  2003 | japan    |
|         2 | atari       |  1972 | US       |
|         3 | naughty_dog |  1984 | US       |
|         4 | syscom      |     0 | NULL     |
|         5 | konami      |  1969 | japan    |
|         6 | phenomic    |  1997 | germany  |
|         7 | nintendo    |  1889 | japan    |
|         8 | game_freak  |  1989 | japan    |
|         9 | ubisoft     |  1986 | france   |
|        10 | sega        |  1960 | japan    |
|        11 | rfx         |  1998 | france   |
|        12 | blizzard    |  1991 | US       |
|        13 | infogrames  |  1983 | france   |
|        14 | activision  |  1979 | US       |
|        15 | grezzo      |  2006 | japan    |
+-----------+-------------+-------+----------+
```



#### select * from games;
```
+------+---------------------+-----------+---------+------+
| id   | name                | studio_id | console | date |
+------+---------------------+-----------+---------+------+
|    1 | xtrem               |         4 | ps2     | 2001 |
|    2 | ff12                |         1 | ps2     | 2006 |
|    3 | xxl2                |         2 | ps2     | 2005 |
|    4 | jak1                |         3 | ps2     | 2001 |
|    5 | jak3                |         3 | ps2     | 2004 |
|    6 | kh                  |         1 | ps2     | 2002 |
|    7 | kh2                 |         1 | ps2     | 2005 |
|    8 | khbbs               |         1 | psp     | 2010 |
|    9 | vpl                 |         1 | psp     | 2007 |
|   10 | ff7cc               |         1 | psp     | 2007 |
|   11 | jak4                |         3 | psp     | 2009 |
|   12 | castlevaniax        |         5 | psp     | 2007 |
|   13 | spellforce          |         6 | pc      | 2003 |
|   14 | warcraft3           |        12 | pc      | 2002 |
|   15 | warcraft3ft         |        12 | pc      | 2003 |
|   16 | pokemon_w2          |         8 | ds      | 2012 |
|   17 | soulsilver          |         8 | ds      | 2010 |
|   18 | platinum            |         8 | ds      | 2008 |
|   19 | diamond             |         8 | ds      | 2006 |
|   20 | spirit_tracks       |         7 | ds      | 2009 |
|   21 | phantom_hourglass   |         7 | ds      | 2007 |
|   22 | mariokart           |         7 | ds      | 2005 |
|   23 | insp_gadget         |        11 | gbc     | 2000 |
|   24 | gb_collection       |         5 | gbc     | 2000 |
|   25 | dkland              |         7 | gbc     | 1995 |
|   26 | links_awakening     |         7 | gbc     | 1993 |
|   27 | smurfs              |        13 | gbc     | 1997 |
|   28 | crystal             |         8 | gbc     | 2000 |
|   29 | silver              |         8 | gbc     | 1999 |
|   30 | rayman              |         9 | gbc     | 2000 |
|   31 | dk_country          |         7 | gbc     | 2000 |
|   32 | yellow              |         8 | gbc     | 1998 |
|   33 | red                 |         8 | gbc     | 1996 |
|   34 | red                 |         8 | gbc     | 1996 |
|   35 | sup_marioland       |         7 | gbc     | 1989 |
|   36 | abugslife           |        14 | gbc     | 1998 |
|   37 | minishcap           |         7 | gba     | 2004 |
|   38 | khcom               |         1 | gba     | 2004 |
|   40 | sonic_adv_2         |        10 | gba     | 2002 |
|   41 | dbz_buus_fury       |         2 | gba     | 2004 |
|   42 | red_mystery_dungeon |         8 | gba     | 2006 |
|   43 | leafgreen           |         8 | gba     | 2004 |
|   44 | sapphire            |         8 | gba     | 2002 |
|   45 | everoasis           |        15 | 3ds     | 2017 |
|   46 | bravely_second      |         1 | 3ds     | 2015 |
|   47 | moon                |         8 | 3ds     | 2016 |
|   48 | ruby_omega          |         8 | 3ds     | 2014 |
|   49 | oot                 |         7 | 3ds     | 2011 |
|   50 | majoras_mask        |         7 | 3ds     | 2015 |
+------+---------------------+-----------+---------+------+
```

### different types of SQL joins

* inner join : matching values in both tables : intersection
* left join (outer) : keep values in left table 
* right join (outer) : keep values in right table
* full join (outer) :  union
* natural join : column with same name and type in 2 tables
* cross join : cartesion product of 2 tables. Often combined with WHERE clause.
* union join : full join but with same columns
* self join : need reflexion. Table on itself

### inner join
* select games.id as game_id, games.name as game_name, games.console, games.date, studios.name as studio_name, studios.birth as studio_birth, studios.location as studio_location from games inner join studios on games.studio_id = studios.studio_id;

```
+---------+---------------------+---------+------+-------------+--------------+-----------------+
| game_id | game_name           | console | date | studio_name | studio_birth | studio_location |
+---------+---------------------+---------+------+-------------+--------------+-----------------+
|       1 | xtrem               | ps2     | 2001 | syscom      |            0 | NULL            |
|       2 | ff12                | ps2     | 2006 | square_enix |         2003 | japan           |
|       3 | xxl2                | ps2     | 2005 | atari       |         1972 | US              |
|       4 | jak1                | ps2     | 2001 | naughty_dog |         1984 | US              |
|       5 | jak3                | ps2     | 2004 | naughty_dog |         1984 | US              |
|       6 | kh                  | ps2     | 2002 | square_enix |         2003 | japan           |
|       7 | kh2                 | ps2     | 2005 | square_enix |         2003 | japan           |
|       8 | khbbs               | psp     | 2010 | square_enix |         2003 | japan           |
|       9 | vpl                 | psp     | 2007 | square_enix |         2003 | japan           |
|      10 | ff7cc               | psp     | 2007 | square_enix |         2003 | japan           |
|      11 | jak4                | psp     | 2009 | naughty_dog |         1984 | US              |
|      12 | castlevaniax        | psp     | 2007 | konami      |         1969 | japan           |
|      13 | spellforce          | pc      | 2003 | phenomic    |         1997 | germany         |
|      14 | warcraft3           | pc      | 2002 | blizzard    |         1991 | US              |
|      15 | warcraft3ft         | pc      | 2003 | blizzard    |         1991 | US              |
|      16 | pokemon_w2          | ds      | 2012 | game_freak  |         1989 | japan           |
|      17 | soulsilver          | ds      | 2010 | game_freak  |         1989 | japan           |
|      18 | platinum            | ds      | 2008 | game_freak  |         1989 | japan           |
|      19 | diamond             | ds      | 2006 | game_freak  |         1989 | japan           |
|      20 | spirit_tracks       | ds      | 2009 | nintendo    |         1889 | japan           |
|      21 | phantom_hourglass   | ds      | 2007 | nintendo    |         1889 | japan           |
|      22 | mariokart           | ds      | 2005 | nintendo    |         1889 | japan           |
|      23 | insp_gadget         | gbc     | 2000 | rfx         |         1998 | france          |
|      24 | gb_collection       | gbc     | 2000 | konami      |         1969 | japan           |
|      25 | dkland              | gbc     | 1995 | nintendo    |         1889 | japan           |
|      26 | links_awakening     | gbc     | 1993 | nintendo    |         1889 | japan           |
|      27 | smurfs              | gbc     | 1997 | infogrames  |         1983 | france          |
|      28 | crystal             | gbc     | 2000 | game_freak  |         1989 | japan           |
|      29 | silver              | gbc     | 1999 | game_freak  |         1989 | japan           |
|      30 | rayman              | gbc     | 2000 | ubisoft     |         1986 | france          |
|      31 | dk_country          | gbc     | 2000 | nintendo    |         1889 | japan           |
|      32 | yellow              | gbc     | 1998 | game_freak  |         1989 | japan           |
|      33 | red                 | gbc     | 1996 | game_freak  |         1989 | japan           |
|      34 | red                 | gbc     | 1996 | game_freak  |         1989 | japan           |
|      35 | sup_marioland       | gbc     | 1989 | nintendo    |         1889 | japan           |
|      36 | abugslife           | gbc     | 1998 | activision  |         1979 | US              |
|      37 | minishcap           | gba     | 2004 | nintendo    |         1889 | japan           |
|      38 | khcom               | gba     | 2004 | square_enix |         2003 | japan           |
|      40 | sonic_adv_2         | gba     | 2002 | sega        |         1960 | japan           |
|      41 | dbz_buus_fury       | gba     | 2004 | atari       |         1972 | US              |
|      42 | red_mystery_dungeon | gba     | 2006 | game_freak  |         1989 | japan           |
|      43 | leafgreen           | gba     | 2004 | game_freak  |         1989 | japan           |
|      44 | sapphire            | gba     | 2002 | game_freak  |         1989 | japan           |
|      45 | everoasis           | 3ds     | 2017 | grezzo      |         2006 | japan           |
|      46 | bravely_second      | 3ds     | 2015 | square_enix |         2003 | japan           |
|      47 | moon                | 3ds     | 2016 | game_freak  |         1989 | japan           |
|      48 | ruby_omega          | 3ds     | 2014 | game_freak  |         1989 | japan           |
|      49 | oot                 | 3ds     | 2011 | nintendo    |         1889 | japan           |
|      50 | majoras_mask        | 3ds     | 2015 | nintendo    |         1889 | japan           |
+---------+---------------------+---------+------+-------------+--------------+-----------------+
```

### left join
```
+---------+---------------------+---------+------+-------------+--------------+-----------------+
| game_id | game_name           | console | date | studio_name | studio_birth | studio_location |
+---------+---------------------+---------+------+-------------+--------------+-----------------+
|       2 | ff12                | ps2     | 2006 | square_enix |         2003 | japan           |
|       6 | kh                  | ps2     | 2002 | square_enix |         2003 | japan           |
|       7 | kh2                 | ps2     | 2005 | square_enix |         2003 | japan           |
|       8 | khbbs               | psp     | 2010 | square_enix |         2003 | japan           |
|       9 | vpl                 | psp     | 2007 | square_enix |         2003 | japan           |
|      10 | ff7cc               | psp     | 2007 | square_enix |         2003 | japan           |
|      38 | khcom               | gba     | 2004 | square_enix |         2003 | japan           |
|      46 | bravely_second      | 3ds     | 2015 | square_enix |         2003 | japan           |
|       3 | xxl2                | ps2     | 2005 | atari       |         1972 | US              |
|      41 | dbz_buus_fury       | gba     | 2004 | atari       |         1972 | US              |
|       4 | jak1                | ps2     | 2001 | naughty_dog |         1984 | US              |
|       5 | jak3                | ps2     | 2004 | naughty_dog |         1984 | US              |
|      11 | jak4                | psp     | 2009 | naughty_dog |         1984 | US              |
|       1 | xtrem               | ps2     | 2001 | syscom      |            0 | NULL            |
|      12 | castlevaniax        | psp     | 2007 | konami      |         1969 | japan           |
|      24 | gb_collection       | gbc     | 2000 | konami      |         1969 | japan           |
|      13 | spellforce          | pc      | 2003 | phenomic    |         1997 | germany         |
|      20 | spirit_tracks       | ds      | 2009 | nintendo    |         1889 | japan           |
|      21 | phantom_hourglass   | ds      | 2007 | nintendo    |         1889 | japan           |
|      22 | mariokart           | ds      | 2005 | nintendo    |         1889 | japan           |
|      25 | dkland              | gbc     | 1995 | nintendo    |         1889 | japan           |
|      26 | links_awakening     | gbc     | 1993 | nintendo    |         1889 | japan           |
|      31 | dk_country          | gbc     | 2000 | nintendo    |         1889 | japan           |
|      35 | sup_marioland       | gbc     | 1989 | nintendo    |         1889 | japan           |
|      37 | minishcap           | gba     | 2004 | nintendo    |         1889 | japan           |
|      49 | oot                 | 3ds     | 2011 | nintendo    |         1889 | japan           |
|      50 | majoras_mask        | 3ds     | 2015 | nintendo    |         1889 | japan           |
|      16 | pokemon_w2          | ds      | 2012 | game_freak  |         1989 | japan           |
|      17 | soulsilver          | ds      | 2010 | game_freak  |         1989 | japan           |
|      18 | platinum            | ds      | 2008 | game_freak  |         1989 | japan           |
|      19 | diamond             | ds      | 2006 | game_freak  |         1989 | japan           |
|      28 | crystal             | gbc     | 2000 | game_freak  |         1989 | japan           |
|      29 | silver              | gbc     | 1999 | game_freak  |         1989 | japan           |
|      32 | yellow              | gbc     | 1998 | game_freak  |         1989 | japan           |
|      33 | red                 | gbc     | 1996 | game_freak  |         1989 | japan           |
|      34 | red                 | gbc     | 1996 | game_freak  |         1989 | japan           |
|      42 | red_mystery_dungeon | gba     | 2006 | game_freak  |         1989 | japan           |
|      43 | leafgreen           | gba     | 2004 | game_freak  |         1989 | japan           |
|      44 | sapphire            | gba     | 2002 | game_freak  |         1989 | japan           |
|      47 | moon                | 3ds     | 2016 | game_freak  |         1989 | japan           |
|      48 | ruby_omega          | 3ds     | 2014 | game_freak  |         1989 | japan           |
|      30 | rayman              | gbc     | 2000 | ubisoft     |         1986 | france          |
|      40 | sonic_adv_2         | gba     | 2002 | sega        |         1960 | japan           |
|      23 | insp_gadget         | gbc     | 2000 | rfx         |         1998 | france          |
|      14 | warcraft3           | pc      | 2002 | blizzard    |         1991 | US              |
|      15 | warcraft3ft         | pc      | 2003 | blizzard    |         1991 | US              |
|      27 | smurfs              | gbc     | 1997 | infogrames  |         1983 | france          |
|      36 | abugslife           | gbc     | 1998 | activision  |         1979 | US              |
|      45 | everoasis           | 3ds     | 2017 | grezzo      |         2006 | japan           |
+---------+---------------------+---------+------+-------------+--------------+-----------------+
```

### right join 
same as inner join

### full join

### cross join

### self join

## Notes
* outer joins preserve rows that don't match
